#!/bin/sh

local="/home/nils/keepass.kdbx"
gdrive="/home/nils/gdrive/keepass/keepass.kdbx"

[ ! -f "$gdrive" -o ! -f "$local" ] && exit 0
diff "$local" "$gdrive" > /dev/null 2>&1 || cp "$local" "$gdrive"

# 0 * * * * /home/nils/scripts/keepass-sync-db.sh
