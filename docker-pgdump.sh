#!/bin/sh

if [ -z "$DIR" -o -z "$HOSTNAME" -o -z "$DATABASE" ]; then
  echo "\$DIR, \$HOSTNAME and \$DATABASE environment variables must be defined" 1>&2
  exit 1
fi

if [ ! -d "$DIR" ]; then
  echo "$DIR: No such directory" 1>&2
  exit 1
fi

date="$(date +%Y-%m-%d:%H:%M:%S)"
pg_dump -h "$HOSTNAME" "$DATABASE" > "$DIR/$DATABASE.$date.sql"
